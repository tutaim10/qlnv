﻿using qlnv.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace qlnv.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeContext db = new EmployeeContext();
        // GET: Employee
        public ActionResult Index()
        {
            return View(db.nhanViens.ToList());
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
       [HttpPost]
        public ActionResult Create([Bind(Include = "ID,EMPLOYEEID,EMPLOYEENAME,DEPARTMENT,SALARY")] NhanVien nhanVien)
        {
            try
            {
                // TODO: Add insert logic 
                db.nhanViens.Add(nhanVien);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }

}